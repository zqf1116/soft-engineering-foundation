import java.util.Scanner;

public class E4 {
    public static void main(String[] args) {
        int[] arr = {12,45,12,46,13,49};


        //排序
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length - i - 1; j++) {
                if(arr[j] > arr[j+1]){
                    int t = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = t;
                }
            }
        }


        for (int j : arr) {
            System.out.print(j + " ");
        }
    }
}
