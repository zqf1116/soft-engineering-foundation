package Test;

import org.junit.jupiter.api.Test;

import static org.junit.Assert.*;

public class RG_Experment3_jia {

    //使用冒泡排序进行测试
    @Test
    public void sort1Test(){

        //基本测试用例
        int [] arr1 = {12,45};
        System.out.println("1.条件覆盖");
        System.out.println("测试if中的arr[j] > arr[j+1]   ->  False");
        System.out.print("  输入用例:");

        dataIn(arr1);
        sort(arr1);
        assertArrayEquals(new int[] {12,45},arr1);
        System.out.println();


        System.out.print("  输出结果:");
        print(arr1);

    }


    @Test
    public void sort2Test(){
        int [] arr2 = {99,8};
        System.out.println("1.条件覆盖");
        System.out.println("测试if中的arr[j] > arr[j+1]   ->  True");
        System.out.print("  输入用例:");

        dataIn(arr2);
        sort(arr2);
        assertArrayEquals(new int[] {99,8},arr2);
        System.out.println();

        System.out.print("  输出结果:");
        print(arr2);
    }

    @Test
    public void sort3Test(){
        int [] arr3 = {7,8,9,11,10};
        System.out.println("1.条件覆盖");
        System.out.println("测试数据，其中至少有一个逆序对");
        System.out.print("  输入用例:");

        dataIn(arr3);
        sort(arr3);
        assertArrayEquals(new int[] {7,8,9,10,11},arr3);
        System.out.println();

        System.out.print("  输出结果:");
        print(arr3);
    }

    @Test
    public void sort4Test(){
        int [] arr4 = {1,2,3,4,5};
        System.out.println("1.条件覆盖");
        System.out.println("数组已经按照升序排号");
        System.out.print("  输入用例:");

        dataIn(arr4);
        sort(arr4);
        assertArrayEquals(new int[]{1,2,3,4,5},arr4);
        System.out.println();

        System.out.print("  输出结果:");
        print(arr4);
    }

    @Test
    public void sort6Test(){
        int [] arr5 = {1};
        System.out.println("1.条件覆盖");
        System.out.println("只有1个元素");
        System.out.print("  输入用例:");

        dataIn(arr5);
        sort(arr5);
        assertArrayEquals(new int[]{1},arr5);
        System.out.println();

        System.out.print("  输出结果:");
        print(arr5);
    }

    @Test
    public void sort7Test(){
        int [] arr5 = {};
        System.out.println("1.条件覆盖");
        System.out.println("没有元素");
        System.out.print("  输入用例:");

        dataIn(arr5);
        sort(arr5);
        assertArrayEquals(new int[]{},arr5);
        System.out.println();

        System.out.print("  输出结果:");
        print(arr5);
    }



    private static void print(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i]+" ");
        }
    }

    public static void sort(int[] arr) {
        //进行排序
        if(arr.length == 1){
            return;
        }
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if(arr[j] > arr[j+1]){
                    int temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                }
            }
        }
    }

    private void dataIn(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i]+" ");
        }
    }

}


